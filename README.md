![alt text][logo]

[logo]: https://www.datocms-assets.com/2885/1518545460-blog-graphics-3.png "Madrid HashiCorp User Group"

# Madrid HashiCorp User Group
Repo para poder planificar y compartir las charlas de Madrid HUG. Acá podrás proponer nuevas charlas y ver los contenidos compartidos de las anteriores.

## Cómo organizamos nuestras charlas y colaboración
Tenemos la intención de realizar un meetup presencial el segundo jueves de cada mes(puede estar sujeto a cambio en base a disponibilidad). Cada meetup sigue la siguiente estructura básica:

*  18:30 - 19:00 : Llegada a lugar y compartir con miembros
*  19:00 - 20:30 : Charlas Disponibles
*  20:30 - --:-- : Compartir con bebidas y cómida y Anuncios(si estas haciendo algun proyecto entretenido o quieres compartir ofertas de trabajo con la comunidad, este es el momento!!)

Nuestros canales digitales

1. [Meetup](https://www.meetup.com/Madrid-HashiCorp-User-Group/)


## ¿Como proponer una charla para un meetup?
1. __Creando un Issue en el repositorio:__ 
  1.  Ingresa a la sección de [issues](https://gitlab.com/hug-madrid/meetup/issues) de el Repositorio
  2.  [Crea un nuevo Issue](https://gitlab.com/hug-madrid/meetup/issues/new), dentro del issue especifica lo siguiente:
* Nombre de la charla
* Nombre del contacto
* Descripción de la charla
* (Opcional) indicar si tienes un blog post, slides, video online para compartir. Idealmente después de que hayas hecho la charla.




## Código de Conducta Comunidad 

### Recomendaciones para construir una comunidad fuerte
1. __Se amable y de mente abierta__
  * Otros colaboradores pueden no tener el mismo nivel de experiencia y contexto que tu, pero eso no significa que no tengan buenas ideas para contribuir. Te alentamos a ser hospitalario a los nuevos colaboradores en sus proyectos y discusiones.
2. __Partir por la base de que no hay mala intención__
  * Los humanos cometemos errores y los desacuerdos y diferencias de opinion son sucesos de la vida. Trata de acercarte a tales conflictos con la perspectiva de que la gente desea hacer algo bueno también. Esto promovera una atmosfera de respeto donde la gente se pueda sentir comoda y hacer preguntas, participar en discusiones y poder hacer contribuciones.
3. __Mantenerse en el tema__
  * La gente participa en esta comunidad para poder aprender y crecer sus habilidades. Los comentarios fuera de tema son distracciones( algunas veces bienvenidas, normalmente no) de lograr hacer el trabajo y ser productivos. Mantenerse en el tema ayuda a producir discusiones positivas y productivas.
4. __Ser claros y concisos__
  * Comunicarse con extraños a través de internet puede ser raro. Es difícil transmitir o entender el tono, y el sarcasmo es frecuentemente mal entendido. Trata de usar lenguaje claro, y piensa como el mensaje será recibido por los otros participantes.

### ¿Qué no está permitido?
1. Amenazas de Violencia
2. Discriminación o discurso ofensivo 
3. Bullying o acoso
4. Invasión de privacidad 
5. Charlas o material con contenido sexualmente obsceno
6. Distrubución de malware o exploits

### ¿Qué sucedera si alguien no sigue las reglas?
1. Eliminación de contenido
2. Bloqueo de usuario en canales digitales
3. No se le permitirá la entrada a los meetups de la Comunidad


